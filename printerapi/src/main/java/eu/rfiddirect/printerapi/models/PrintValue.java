package eu.rfiddirect.printerapi.models;

public class PrintValue {
    public int Id;
    public String Content;

    public int getId() {
        return Id;
    }

    public String getContent() {
        return Content;
    }

    public void setId(int id) {
        Id = id;
    }

    public void setContent(String content) {
        Content = content;
    }
}
