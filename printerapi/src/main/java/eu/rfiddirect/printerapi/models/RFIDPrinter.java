package eu.rfiddirect.printerapi.models;

public class RFIDPrinter {
    public int Id;
    public String IpAddress;
    public int PrintPort;
    public int StatusPort;
    public String PrinterTemplate;
    public String Name;

    public RFIDPrinter(String printerIp, int printerPort, int statusPort, String templateName) {
        this.IpAddress = printerIp;
        this.PrinterTemplate = templateName;
        this.PrintPort = printerPort;
        this.StatusPort = statusPort;
    }

    public void setPrintPort(int printPort) {
        PrintPort = printPort;
    }

    public void setStatusPort(int statusPort) {
        StatusPort = statusPort;
    }

    public void setPrinterTemplate(String printerTemplate) {
        PrinterTemplate = printerTemplate;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setId(int id){
        Id = id;
    }

    public void setIpAddress(String ipAddress){
        IpAddress = ipAddress;
    }

    public int getId() {
        return Id;
    }

    public String getIpAddress() {
        return IpAddress;
    }

    public int getPrintPort() {
        return PrintPort;
    }

    public int getStatusPort() {
        return StatusPort;
    }

    public String getPrinterTemplate() {
        return PrinterTemplate;
    }

    public String getName() {
        return Name;
    }
}
