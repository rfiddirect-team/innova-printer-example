package eu.rfiddirect.printerapi;

import eu.rfiddirect.printerapi.models.PrintValue;
import eu.rfiddirect.printerapi.models.RFIDPrinter;
import eu.rfiddirect.printerapi.zebra.PrinterHelper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@RestController
public class PrinterapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrinterapiApplication.class, args);
    }

    @GetMapping("/hello")
    public String sayHello(@RequestParam(value = "myName", defaultValue = "World") String name) {
        return String.format("Hello %s!", name);
    }

    @GetMapping("/print")
    public ResponseEntity printLabel(
        @RequestParam(value="printerIp") String printerIp,
        @RequestParam(value="printerPort") int printerPort,
        @RequestParam(value="statusPort") int statusPort,
        @RequestParam(value="templateName") String templateName,
        @RequestParam(value="values")ArrayList<PrintValue> values
    ){
        boolean success = true;
        RFIDPrinter printer = null;
        Map<Integer, String> vars = new HashMap<Integer, String>();
        try {
            printer = new RFIDPrinter(printerIp, printerPort, statusPort, templateName);

            for (PrintValue printValue : values) {
                vars.put(printValue.Id, printValue.Content);
            }
        } catch (Exception e){
            success = false;
        }
        if (success) {
            success = PrinterHelper.Print(printer, vars);
        }
        return ResponseEntity.ok(success);
    }
}
