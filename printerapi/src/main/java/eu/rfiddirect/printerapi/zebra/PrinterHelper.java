package eu.rfiddirect.printerapi.zebra;


import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.comm.MultichannelTcpConnection;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;
import eu.rfiddirect.printerapi.models.RFIDPrinter;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public class PrinterHelper {

    private static Connection GetConnection(RFIDPrinter printer)
    {
        return new MultichannelTcpConnection(printer.IpAddress, printer.PrintPort, printer.StatusPort);
    }

    public static boolean Print(RFIDPrinter printer, Map<Integer, String> values)
    {
        boolean success = true;
        Connection printerConnection = null;
        try {
            printerConnection = GetConnection(printer);
            printerConnection.open();
            ZebraPrinterFactory.getInstance(printerConnection).printStoredFormat(printer.PrinterTemplate, values, "utf8");
        } catch (UnsupportedEncodingException e) {
            success = false;
        } catch (ConnectionException e) {
            success = false;
            e.printStackTrace();
        } catch (ZebraPrinterLanguageUnknownException e) {
            success = false;
            e.printStackTrace();
        } finally {
            if (printerConnection != null){
                try {
                    printerConnection.close();
                } catch (ConnectionException e) {
                    success = false;
                    e.printStackTrace();
                }
            }
        }
        return success;
    }
}
